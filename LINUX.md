# ADMINISTRATION LINUX

## SOMMAIRE

```yaml
---

# Exemple de code :)

- pwet
```

1.[Commandes de bases](#commandes)

2.[Grub](#grub)

2.1[Fichiers de Démarrage](#fichiers)

2.2[SystemD](#systemd)

3.[Password](#password)

4.[Maintenance via Grub](#maintenancegrub)

4.1[Avec Pass Root](#avec-mot-de-passe-root)

## COMMANDES DE BASES

Liste de commandes basiques

- `find`
- `cat`
- `echo`
- `ls`
- `mkdir`
- `touch`
- `rm`
- `rmdir`
- `sudo`
- `less`
- `wc`
- `tee`
- `grep`
- `tail`
- `head`


## **GRUB**grub

Modification de la configuration de GRUB à partir de :

- `/etc/default/grub`
- `/etc/grub.d/`

Regénérer le fichier GRUB à l'aide de `grub-mkconfig` et `update-grub`

*ex:*

> `grub-mkconfig -o /boot/grub/grub.cfg` ou `update-grub`

### *Fichiers de démarrage*fichiers

Fichier indispensables pour le démarrage minimum de Debian:

    `ls -l /boot/`  => vmlinuz-x.x.x & initrd.img-x.x.x

Ligne pouvant être modifier (au démarrage, touche 'e')

    `linux /vm-linuz-4.x.x.x-x-amd64 root=/dev/mapper/debian--vg-root ro quiet`

## **SystemD**systemd

SystemD est le gestionnaire système, PID 1.

Pour gouverner tous les services:

```sh
# Mon commentaire
systemctl <action> [cible/service/unit]
```

-Afficher la cible par défaut

-Changer de cible par défaut

-Changer de cible directement

-Gérer les services


------ Lister les cibles, services et autres éléments de SystemD ------

Connaître la cible par défaut

    `systemctl get-default`

Changer la cible par défaut

    `systemctl set-default multi-user.target`

Changer de cible

    `systemctl isolate rescue-target`

Lister les divers éléments (units) de SystemD

    `systemctl list-units`


Lister tous les units

    `systemctl list-units --all`

Voir le statut d'un service

    `systemctl status cron.service`

Démarrer un service

    `systemctl start cron.service`

Stopper un service

    `systemctl stop cron.service`

Redémarrer un service

    `systemctl restart cron.service`

Activer un service

    `systemctl enable networking`

Désactiver un service

    `systemctl disable networking`


## **PASSWORD**password

Change user's password:

    `sudo passwd user`


## **MAINTENANCE VIA GRUB**maintenancegrub

### avec-mot-de-passe-root> ***AVEC MOT DE PASSE ROOT***</a>
Passer par le boot manager GRUB pour passer en mode Maintenance
Utiliser les flèches pour choisir la ligne du noyau voulu puis appuyez sur [e]

Ligne à modifier:

    `linux /vm-linuz-4.x.x.x-x-amd64 root=/dev/mapper/debian--vg-root ro quiet`

en

    `linux /vm-linuz-4.x.x.x-x-amd64 root=/dev/mapper/debian--vg-root ro single`

Valider avec CTRL+X ou [F10] Saisir le mot de passe ROOT.
Après les actions en mode maintenance, CTRL+D > rescue.target


### ***SANS MOT DE PASSE ROOT***

Mêmes étapes que #AVEC MOT DE PASSE ROOT

    `linux /vm-linuz-4.x.x.x-x-amd64 root=/dev/mapper/debian--vg-root ro init=/bin/bash`

// Connexion en root sans mot de passe

Système est démarré mais système de fichier racine est en lecture seule:

    `mount -o remount, rw /` > Permet d'ajouter l'écriture

Après actions, synchro écriture de données en RAM vers DISK avec command:

    `sync`

/!\ Reboot impossible, extinction par POWEROFF matériel /!\

## [**GESTION RESEAUX**](#gestion-reseaux)

    `ip a`  Connaître IP ADDRESS
    `ip r`  r = Route  Connaître la passerelle par défaut

Informations DNS:

     `/etc/resolv.conf`
(Possibilité de le modifier pour mettre  un DNS statique)

### ***CONFIGURATION IP EN DHCP***confipdhcp

Fichier pour gérer la partie IP:

    `/etc/network/interfaces`
Edition du fichier (ne jamais commenter ou effacer les ligne loopback):

    `vim /etc/network/interfaces`

Configuration statique:
    - Modification de la ligne:

    `iface ens33 inet dhcp` en:
    `iface ens33 inet static`
        address: 10.1.1.10
        netmask: 255.255.0.0
        gateway: 10.1.255.251

(Possibilité de renseigner l'address avec CIDR /16 dans cette exemple sans renseigner le netmask)

Si l'IP statique n'est pas prise en compte ajouter la ligne suivante au-dessus:

    `auto ens33`

Gérer le service networking

    `systemctl stop/start/restart networking.service`

### ***CONFIGURATION IP VIA INTERFACE GRAPHIQUE***confipgraph

Network Manager  Clic droit sur l'icone en haut à droite
     Paramètres Filaire


    `systemctl restart NetworkManager` ou via interface graphique


## **GESTION DES PAQUETS LOGICIELS**paquets

### ***GESTION DES DEPÔTS***depots

Fichier de configuration des dépôts:

    `/etc/apt/sources.list`

Modification du fichier avec toutes les sources à disposition:

    `deb http://ftp.fr.debian.org/ buster main contrib non-free`
    `deb-src http://ftp.fr.debian.org/ buster main contrib non-free`

Lancement mise à jour des paquets (root)

    `apt update` > relecture des paquets (vérification des versions)
    `apt upgrade`  met à jour les nouveaux paquets
    `apt full-upgrade`  Supprimes les paquets obsolètes et installe les nouveaux

Installation des VMWARE TOOLS:

    `apt install open-vm-tools-desktop`

Resélectionner les utilitaires comme à l'installation:

    `tasksel`

Rechercher un paquet en particulier (ftp par exemple):

    `apt search ftp`

## **GESTION DES ESPACES DE STOCKAGE**espacestockage

La commande `fdisk`

    `fdisk <option> [périphérique de stockage]`
Afficher la table de partion du périphérique

    `fdisk -l /dev/sdb`
Modifier les partitions du périphérique sélectionné

    `fdisk /dev/sdb`

Obtenir une info d'un disque lorsqu'il n'est pas encore monté: (hôte)

    `udevadm info --query=path --name=sda`
    `echo "- - -" > /sys/class/scsi_host/host#/scan`
    `fdisk -l`

## **GESTION AVANCEE DES ESPACES DE STOCKAGES - LVM**lvm
Création de volume physique

    pvcreate
    pvcreate /dev/sdb1 /dev/sdb2 /dev/sdc1 /dev/sdd

Création des groupes de volumes

    vgcreate
    vgcreate NomDuGroupe /dev/sdb1 /dev/sdb2 /dev/sdc1

Création des volumes logiques

    lvcreate
    lvcreate -n lv1 -L 10G NomDuGroupe

Les options `-n` et `-L` sont respectivement le nom et la taille.

Deux chemins pour manipuler les Volumes Logiques:

    /dev/vgsystem/lvhome
ou

    /dev/mapper/vgsystem-lvhome

Ajouter des volumes physiques au VG

    vgextend NomDuGroupe /dev/LABEL

Agrandir un Volume Logique: (`-r` = invoque resize2fs / `-L` = Taille)

    lvextend -r -L +512M /dev/NomDuGroupe/LABEL
    lvextend -r -L 1G /dev/NomDuGroupe/LABEL2

Réduire un Volume Logique

    lvreduce -r -L +512M /dev/NomDuGroupe/LABEL

Etendre un volume en prenant tout l'espace libre

    lvextend -l +100%FREE /dev/NomDuGroup/HDD-LABEL

### ***INFORMATIONS LVM***

Toutes les commandes LVM sont disponibles dans un shell dédié, accessible via la commande `lvm`

| Informations Résumées avec `s` | Infos Détaillés avec `display` |  Designation |
| ------------------------------ | ------------------------------ | ---------------------- |
| `pvs` | `pvdisplay` | Volume Physique
| `vgs` | `vgdisplay` | Groupe de volume
| `lvs` | `lvdisplay` | Volume Logique

Exemples:

    vgdisplay vggroup2

    lvdisplay /dev/vggroup2/lv1

    pvdisplay /dev/sdb1

## **[GESTION DES ESPACES DE STOCKAGES - FILE SYSTEM](#gestion-des-espaces-de-stockages---file-system)**

**Création de systèmes de fichiers:**

    mkfs.[fstype] <option> [périphérique de stockage]

Exemples:
- Création d'un système de fichiers ext4 sur partition /dev/sda2
    -  `mkfs.ext4 /dev/sda2`
- Création d'un système de fichiers ext4 sur volume logique lv1
    - `mkfs.ext4 /dev/vggroup1/lv1`
- Création d'un système de fichiers NTFS (nécessite le paquet `ntfs-3g`)
    - `mkfs.ntfs /dev/sde1`

**Modification d'un système de fichiers:**

    tune2fs <options> [périphérique de stockage]

Options usuelles:
  - `-L` permet de modifier l'étiquette du système de fichiers
  - `-l` permet d'afficher les informations du superbloc
  - `-i` permet de modifier l'intervalle entre deux vérifications
  - `-c` permet de modifier le nombre maximum de montages déclenchant une vérification

Commande de changement de taille d'un système de fichier:

    resize2fs <options> [périphériques de stockage]

**Vérification d'un système de fichiers**

    fsck.<fstype> <options> [périphérique de stockage]

**Prise d'information**

    blkid <options> [périphérique de stockage]

Cette commande permet d'afficher les informations relatives à un périphériques particulier. Sans argument, elle affichera les informations relatives à tous les périphériques formatés.

    lsblk <options> [périphérique de stockage]

La commande `lsblk` permet d'afficher sous forme arborescente les informations relatives aux périphériques et systèmes de fichiers

### MONTAGE D'UN SYSTEME DE FICHIERS

    mount <options> [périphérique source] [/point/de/montage]

Options possibles:
  - `-t [fstype] `Détermine le type de système de fichier à monter
  - `-o [option]` Permet de définir différentes options séparées par une virgule
  - `sync/async` Active ou non l'utilisation de la mise en tampon RAM des données avant écriture dans le système de fichiers. Par défaut: `async`
  - `exec/noexec` Active la possibilité d'exécuter des fichiers exécutables présents sur le systèmes de fichiers
  - `ro/rw` Monte le système de fichiers en lecture seule ou en lecture/écriture
  - `suid/nosuid` Active la possibilité d'exécuter les binaires avec l'interprétation du SUID positionné au dessus
  - `remount` Permet de changer une ou des options de montage sans démonter le système de fichiers

Exemple:

    ls /mnt
    mount -t ext4 /dev/sdc1 /mnt

**Informations sur les montages**

    mount
    findmnt [/chemin/du/volume]

**Démonter un volume**

    unmount /mnt


### MONTAGE AUTOMATIQUE

Le montage automatique est géré par `systemd` au démarrage.
La déclaration des montages automatiques est présente dans le fichier `/etc/fstab`

**Informations sur les systèmes de fichiers**

    df <options> [/point/de/montage]

Commande permettant d'obtenir des informations utiles sur les systèmes de fichiers montés

Options usuelles:

    -h :affiche la taille en puissance de 1024
    -i :affiche les informations sur les inodes

Pour connaître la taille d'un répertoire

    du <options> [/point/de/montage]

Options usuelles:

    -h : affiche la taille en puissance de 1024
    -s : n'affiche pas les sous-répertoires mais uniquement le répertoire en argument

Exemple: `du -hs /etc`

